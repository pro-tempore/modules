package main

import (
	"bytes" // Bytes.buffer is used when creating the Text/Template
	"encoding/json"
	"flag" // Used for the start call (I.E ./Module -server 127.0.0.1)
	"log"  // For log outputs.
	// RPC (Remote Procedure Calls)
	"os"

	"gitlab.com/pro-tempore/modules/base"
	"gitlab.com/pro-tempore/server/types" // The projects unversal types which includes the Table-structs.
	//"reflect"
	"text/template" // Every module should only create a text, not a full html. this is the job of the module.Handler
)

type Timelogs struct {
	base.ModuleBase
}

const (
	insertGeo = "INSERT INTO geodata (operation, time, latitude, longitude, email) VALUES ($1, $2, $3, $4, $5)"
	geoSchema = `CREATE TABLE geodata (operation text, time text, latitude text, longitude text, email text)`
	MyPort    = "30096"
)

var (
	Template = template.Must(template.ParseFiles("Templates/Timelogs.gohtml"))
)

func main() {
	log.SetPrefix("[Module.Timelogs] ") // For logging purposes
	flag.Parse()                        // Parses the flag (if it exists) for example to use flag: go run module.go -server IPADRESS
	instance := new(Timelogs)           // Creates an instance of the program

	mBase := base.New(instance, MyPort)
	err := mBase.Start("Timelogs")
	log.Println(err)
}

//func (e *Timelogs) Root(request url.Values, response *types.RpcResponse) error {
func (t *Timelogs) Root(request types.RpcRequest, response *types.RpcResponse) error {
	var out bytes.Buffer
	userName := request.Cookies["name"]
	userNameString, typingError := userName.(string)
	if !typingError {
		log.Fatalln("Problem parsing username from cookie")
	}
	/** RPC DIAL */
	rows, err := t.Query("SELECT * FROM Geodata WHERE email=$1", request.Cookies["email"].(string))
	if err != nil {
		log.Fatalln("Database error:", err)
	}

	data := make([]types.Geodata, 0)
	timestamp := new(types.Geodata)
	for _, r := range rows {
		base.ParseStruct(r, timestamp)
		data = append(data, *timestamp)
	}

	dataAsJSON, err := json.Marshal(&data)

	varmap := map[string]interface{}{ // This variable is used to get data into the template. for example populate tables and such according to the text template examples.
		"timelogs":     data, // Here we add an variable "timelogs" with the data ([]types.Geodata)
		"timelogsJSON": string(dataAsJSON),
		"name":         userNameString,
	}
	/** TEMPLATE EXECUTION */
	err = Template.ExecuteTemplate(&out, "content", varmap) // In this case the template is rendered. the varmap can be modified to fill out the template with data.
	if err != nil {
		log.Println(err)
	}
	response.Body = out.Bytes() // Generate the []byte which is inserted into the response.Body which is sent through RPC.

	return nil
}

func (t *Timelogs) NewTimestamp(request types.RpcRequest, response *types.RpcResponse) error {
	// for API methods. Define your own, as many as you want.
	log.Println(string(request.Body))
	data := new(types.Geodata)
	err := json.Unmarshal(request.Body, data)

	_, err = t.Query(insertGeo, data.Operation, data.Time, data.Latitude, data.Longitude, data.Email)
	if err != nil {
		log.Println("Database error:", err)
		return err
	}

	response.StatusCode = 200
	log.Println(request.Form["email"])
	return nil
}

func (t *Timelogs) Shutdown(token string, dont_care *bool) error {
	// Do shutdown things here. Required.
	log.Println("Got call to shutdown!")
	os.Exit(0)
	return nil
}

func (t *Timelogs) SetBase(b base.ModuleBase) {
	t.ModuleBase = b
}
