package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/jung-kurt/gofpdf"
	"gitlab.com/pro-tempore/modules/base"
	"gitlab.com/pro-tempore/server/types"
)

const (
	MyPort = "30093"
)

type PDFGen struct {
	base.ModuleBase
}

type Pdfrow struct {
	Name  string `json:"name"`
	Price string `json:"price"`
	Hours string `json:"hours"`
}

func main() {
	flag.Parse()

	instance := new(PDFGen)
	mBase := base.New(instance, MyPort)
	err := mBase.Start("PDFGen")
	log.Println(err)
}

func (p *PDFGen) Root(request types.RpcRequest, response *types.RpcResponse) error {
	pdf := gofpdf.New("P", "mm", "A4", "")

	pdf.SetFooterFunc(func() {
		pdf.SetY(-15)
		pdf.SetFont("Arial", "I", 8)
		pdf.SetTextColor(128, 128, 128)
		pdf.CellFormat(0, 10, fmt.Sprintf("Page %d", pdf.PageNo()),
			"", 0, "C", false, 0, "")
	})
	chapterTitle := func(titleStr string) {
		pdf.SetFont("Arial", "", 12)
		pdf.SetFillColor(200, 220, 255)
		pdf.CellFormat(0, 6, fmt.Sprintf("%s", titleStr),
			"", 1, "L", true, 0, "")
		pdf.Ln(4)
	}
	chapterBody := func(name, time, pay string) {
		pdf.SetFont("Helvetica", "", 12)
		pdf.WriteAligned(0, 6, name, "L")
		pdf.WriteAligned(0, 6, time, "C")
		pdf.WriteAligned(0, 6, pay, "R")
		pdf.Ln(-1)
	}
	printChapter := func(titleStr string) {
		pdf.AddPage()
		chapterTitle(titleStr)
	}
	printSum := func(sum string) {
		pdf.SetFont("Helvetica", "", 12)
		pdf.Line(10, 45, 210-10, 45)
		pdf.Ln(-1)
		chapterBody("", "", "Sum: "+sum)
	}

	printChapter("Faktura " + time.Now().String())

	input := make([]Pdfrow, 0)
	err := json.Unmarshal(request.Body, &input)
	log.Println(err)
	log.Print("Received ")
	log.Println(input)
	sum := 0
	for _, m := range input {
		a, _ := strconv.Atoi(m.Hours)
		b, _ := strconv.Atoi(m.Price)
		chapterBody(m.Name, m.Hours+"h * "+m.Price+"kr/h", strconv.Itoa(a*b))
		sum += a * b
	}

	printSum(strconv.Itoa(sum))

	pdf.OutputFileAndClose("hello.pdf")
	file, _ := ioutil.ReadFile("hello.pdf")

	response.Header = http.Header{}
	response.Header.Add("Content-Type", "application/pdf")
	response.Header.Add("Content-Disposition", "attachment; filename=test.pdf")
	response.StatusCode = 200
	response.Body = file

	os.Remove("hello.pdf")

	return nil
}

func (p *PDFGen) Shutdown(token string, dont_care *bool) error {
	log.Println("Shutting down...")
	os.Exit(0)
	return nil

}
func (p *PDFGen) SetBase(b base.ModuleBase) {
	p.ModuleBase = b
}
