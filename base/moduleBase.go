package base

import (
	"flag"
	"log"
	"net"
	"net/rpc"
	"os"
	"os/signal"
	"reflect"
	"strings"
	"syscall"

	"gitlab.com/pro-tempore/server/types"
)

type Module interface {
	Root(types.RpcRequest, *types.RpcResponse) error
	Shutdown(string, *bool) error
	SetBase(ModuleBase)
}

type ModuleBase struct {
	Server   string
	Port     string
	ID       types.Identity
	instance Module
}

var (
	server = flag.String("server", "127.0.0.1", "Address to main server")
	local  = flag.String("local", "", "Local address")
)

const (
	RPCPort = "30001"
	DBPort  = "30002"
)

func New(instance Module, port string) *ModuleBase {
	out := new(ModuleBase)
	out.Port = port
	out.SetInstance(instance)
	out.Server = *server
	return out
}

func (m *ModuleBase) Start(name string) (err error) {
	log.SetPrefix("[Module." + name + "] ")

	var ip string
	if *local == "" {
		i, ok := GetIP()
		if !ok {
			log.Fatal("Failed to get IP!")
		}
		ip = i
	} else {
		ip = *local
	}
	m.ID.Name = name
	m.ID.Address = ip + ":" + m.Port

	client, err := rpc.Dial("tcp", m.Server+":"+RPCPort)
	if err != nil {
		log.Println("Failed to dial server:", err)
		return
	}

	var dont_care bool // None of the initial calls return anything interesting.
	err = client.Call("Main.Register", m.ID, &dont_care)
	if err != nil {
		log.Println("Registration failed:", err)
		return
	}

	s := rpc.NewServer()
	err = s.RegisterName(m.ID.Name, m.instance)
	if err != nil {
		client.Call("Main.Deregister", m.ID, &dont_care) // deregister on fail.
		log.Println("Failed to register instance:", err)
		return
	}
	listener, err := net.Listen("tcp", m.ID.Address)
	if err != nil {
		client.Call("Main.Deregister", m.ID, &dont_care)
		log.Println("Failed to start TCP listener:", err)
		return
	}
	log.Println("Registered and listening!")
	client.Close()
	// Prep for graceful shutdown
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	signal.Notify(c, syscall.SIGTERM)
	go func() {
		<-c
		m.instance.Shutdown("local", &dont_care)
		log.Println("Caught ^C, Deregistering and exiting...")
		closer, _ := rpc.Dial("tcp", m.Server+":"+RPCPort)
		closer.Call("Main.Deregister", m.ID, &dont_care)
		closer.Close()
		log.Println("Done!")
		os.Exit(1)
	}()

	s.Accept(listener) // listen for incoming method calls. This blocks.
	return
}

func (m *ModuleBase) SetInstance(i Module) {
	m.instance = i
}

func (m *ModuleBase) Query(cmd string, args ...interface{}) (out []map[string]interface{}, err error) {
	client, err := rpc.Dial("tcp", m.Server+":"+DBPort)
	if err != nil {
		return
	}
	query := types.NewQuery(cmd, args...)

	err = client.Call("DB.Query", query, &out)
	return

}

var byteSlice = reflect.TypeOf([]byte{})
var stringType = reflect.TypeOf("")

func ParseStruct(data map[string]interface{}, result interface{}) {
	// Holy shit how does this work...
	s := reflect.ValueOf(result).Elem() // the struct itself
	t := reflect.TypeOf(result).Elem()  // field identifiers
	for i := 0; i < s.NumField(); i++ {
		field := s.Field(i)
		fType := t.Field(i)
		name := strings.ToLower(fType.Name) // the database keys are always lowercase
		item, ok := data[name]
		// Fields that are in the struct but not in the map error out and are skipped.
		// Fields that are in the map but not in the struct never turn up.
		if ok {
			value := reflect.ValueOf(item)
			if value.Type() == byteSlice && fType.Type == stringType {
				// v is a string that's been botched in transit.
				value = value.Convert(stringType)
			}
			field.Set(value)
		}
	}
}

func GetIP() (out string, ok bool) {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		log.Println(err)
		return
	}
	for _, addr := range addrs {
		if ipnet, ok := addr.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				out = ipnet.IP.String()
				break
			}
		}
	}
	ok = out != ""
	return
}
