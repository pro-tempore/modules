package main

import (
	"bytes"
	"flag"
	"log"
	"net/http"
	"os"
	"text/template"
	"time"

	"github.com/kiwih/goical"
	"gitlab.com/pro-tempore/modules/base"
	"gitlab.com/pro-tempore/server/types"
)

type Schedule struct {
	base.ModuleBase
}

const (
	MyPort   = "30098"
	longform = "Jan 2, 2006 at 3:04pm (MST)"
)

var (
	T = template.Must(template.ParseFiles("Templates/Schedule.gohtml"))
)

type Event struct {
	UId            string
	StartTime      time.Time
	EndTime        time.Time
	OrganizerName  string
	OrganizerEmail string
	Location       string
	Summary        string
}

func (e *Event) GetUId() string {
	return e.UId
}

func (e *Event) GetStartTime() time.Time {
	return e.StartTime
}

func (e *Event) GetEndTime() *time.Time {
	return &e.EndTime
}

func (e *Event) GetOrganizerName() string {
	return e.OrganizerName
}

func (e *Event) GetOrganizerEmail() string {
	return e.OrganizerEmail
}

func (e *Event) GetLocation() string {
	return e.Location
}

func (e *Event) GetSummary() string {
	return e.Summary
}

func main() {

	flag.Parse()

	instance := new(Schedule)
	mBase := base.New(instance, MyPort)
	err := mBase.Start("Schedule")
	log.Println(err)
}

func (s *Schedule) Root(request types.RpcRequest, response *types.RpcResponse) error {
	// serve page for module here
	log.Println("Got request for Root!")
	var out bytes.Buffer
	err := T.ExecuteTemplate(&out, "content", "")
	if err != nil {
		log.Println(err)
	}

	response.Body = out.Bytes()

	return nil
}

func (s *Schedule) Calendar(request types.RpcRequest, response *types.RpcResponse) error {
	name := request.Form["name"]
	if len(name) == 0 {
		var out bytes.Buffer
		err := T.ExecuteTemplate(&out, "content", "")
		if err != nil {
			log.Println(err)
		}
		response.Body = out.Bytes()
		return nil
	}
	file := s.makeSchedule(name[0])
	if file == "err" {
		var out bytes.Buffer
		err := T.ExecuteTemplate(&out, "content", "")
		if err != nil {
			log.Println(err)
		}
		response.Body = out.Bytes()
	} else {
		response.Header = http.Header{}
		response.Header.Add("Content-Type", "text/calendar")
		response.Header.Add("Content-Disposition", "attachment; filename="+name[0]+".ical")
		response.StatusCode = 200
		response.Body = []byte(file)
	}
	return nil
}

func (s *Schedule) makeSchedule(name string) string {
	log.SetPrefix("[Schedule.makeSchedule]: ")
	log.Println("Making new schedule.")

	//rows, err := s.Query("SELECT * FROM schedule_events WHERE username=$1", name)
	rows, err := s.Query("SELECT * FROM schedule_events")

	if err != nil {
		log.Fatalln("Database error:", err)
	}

	events := make([]Event, 0)

	e := new(fakeEvent)
	log.Printf("Got %d rows.", len(rows))
	for _, r := range rows {
		base.ParseStruct(r, e)
		events = append(events, e.ToReal())
	}

	ical := goical.NewIcal()
	for _, ev := range events {
		log.Println("Adding calendar for uID " + ev.UId)
		ical.AddEvent(&ev)
	}
	buffer := new(bytes.Buffer)
	if err := ical.Write(buffer); err != nil {
		log.Fatal("Failed to write calendar to buffer:", err)
	}
	output := string(buffer.Bytes())
	return output
}

func (s *Schedule) Shutdown(token string, dont_care *bool) error {
	// do shutdown things here. Required
	log.Println("Got call to shutdown!")
	os.Exit(0)
	return nil
}

func (s *Schedule) SetBase(b base.ModuleBase) {
	s.ModuleBase = b
}

type fakeEvent struct {
	UId           string
	StartTime     string
	EndTime       string
	OrganizerName string
	Location      string
	Summary       string
}

func (f *fakeEvent) ToReal() Event {
	st, _ := time.Parse(longform, f.StartTime)
	et, _ := time.Parse(longform, f.EndTime)
	return Event{
		UId:            f.UId,
		StartTime:      st,
		EndTime:        et,
		OrganizerName:  f.OrganizerName,
		OrganizerEmail: "info@" + f.OrganizerName + ".com",
		Location:       f.Location,
		Summary:        f.Summary,
	}
}
