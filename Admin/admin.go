package main

import (
	"bytes"
	"encoding/json"

	"flag"
	"html/template"
	"log"
	"net/rpc"
	"os"

	"gitlab.com/pro-tempore/modules/base"
	"gitlab.com/pro-tempore/server/types"
)

type Admin struct {
	base.ModuleBase
}

var (
	tUsers   = template.Must(template.ParseFiles("Templates/users.gohtml"))
	tNewUser = template.Must(template.ParseFiles("Templates/new-user.gohtml"))
	tNewRole = template.Must(template.ParseFiles("Templates/new-role.gohtml"))
)

const (
	insertUser = "INSERT INTO users (name, lastname, password, email, company, role, registertime, phone, address, address2, city, region, zip, country) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14,)"
	insertRole = "INSERT INTO roles (name, description, modules) VALUES ($1, $2, $3)"
)

var (
	MyPort = "30097"
)

func main() {
	flag.Parse()

	instance := new(Admin)
	mBase := base.New(instance, MyPort)
	err := mBase.Start("Admin")
	log.Println(err)
}

func (a *Admin) Root(request types.RpcRequest, response *types.RpcResponse) error {
	log.Println("Got a request for root method!")
	var out bytes.Buffer

	client, err := rpc.Dial("tcp", a.Server+":"+base.DBPort)

	if err != nil {
		log.Println("Failed to dial server:", err)
		return err
	}

	query := types.NewQuery("SELECT uid, name, lastname, email, phone, role FROM users")
	rows := make([]map[string]interface{}, 0)

	err = client.Call("DB.Query", query, &rows)
	if err != nil {
		log.Fatalln("Database error:", err)
	}

	data := make([]types.User, 0)
	u := new(types.User)
	for _, r := range rows {
		base.ParseStruct(r, u)
		data = append(data, *u)
	}

	varmap := map[string]interface{}{ // This variable is used to get data into the template. for example populate tables and such according to the text template examples.
		"users": data, // Here we add an variable "timelogs" with the data ([]types.Geodata)
	}
	err = tUsers.ExecuteTemplate(&out, "content", varmap)
	if err != nil {
		log.Println(err)
	}

	response.Body = out.Bytes()

	return nil
}

func (a *Admin) NewUser(request types.RpcRequest, response *types.RpcResponse) error {

	log.Println("Got request for NewUser")

	//Create the new user struct.
	newUser := new(types.User)

	//Unmarshal the json from the rpc request.
	err := json.Unmarshal(request.Body, newUser)
	if err != nil {
		log.Println("Error unmarshaling user in request: ", err)
	}

	//Prepare the query.
	query := types.NewQuery(insertUser,
		newUser.Name,
		newUser.Lastname,
		string(newUser.Password),
		newUser.Email,
		newUser.Company,
		newUser.Role,
		newUser.Registertime,
		newUser.Phone,
		newUser.Address,
		newUser.Address2,
		newUser.City,
		newUser.Region,
		newUser.Zip,
		newUser.Country,
	)

	//Dial the database.
	client, err := rpc.Dial("tcp", a.Server+":"+base.DBPort)
	if err != nil {
		log.Println("Failed to dial server:", err)
		return err
	}
	// we need to send something ,but we don't care about the result.
	ignore := make([]map[string]interface{}, 0)

	//Execute the query
	err = client.Call("DB.Query", query, &ignore)
	if err != nil {
		log.Println("Database error:", err)
		return err
	}

	//Execute the template and return it in the response body.
	var out bytes.Buffer
	err = tNewUser.ExecuteTemplate(&out, "content", nil)
	if err != nil {
		log.Println(err)
		return err
	}
	response.Body = out.Bytes()

	return nil
}

func (a *Admin) NewRole(request types.RpcRequest, response *types.RpcResponse) error {

	log.Println("Got request for NewRole")

	//Create the new user struct.
	newRole := new(types.Roles)

	//Unmarshal the json from the rpc request.
	err := json.Unmarshal(request.Body, newRole)
	if err != nil {
		log.Println("Error unmarshaling role in request: ", err)
	}

	//Prepare the query.
	query := types.NewQuery(
		insertRole,
		newRole.Name,
		newRole.Description,
		newRole.Modules,
	)

	//Dial the database.
	client, err := rpc.Dial("tcp", a.Server+":"+base.DBPort)
	if err != nil {
		log.Println("Failed to dial server:", err)
		return err
	}

	//Execute the query
	values := make([]map[string]interface{}, 0)
	err = client.Call("DB.Query", query, &values)
	if err != nil {
		log.Println("Database error:", err)
		return err
	}

	//Execute the template and return it in the response body.
	var out bytes.Buffer
	err = tNewRole.ExecuteTemplate(&out, "content", nil)
	if err != nil {
		log.Println(err)
		return err
	}
	response.Body = out.Bytes()

	return nil

}

func (a *Admin) Shutdown(token string, dont_care *bool) error {
	// do shutdown things here. Required
	log.Println("Got call to shutdown!")
	os.Exit(0)
	return nil
}

func (a *Admin) SetBase(b base.ModuleBase) {
	a.ModuleBase = b
}
