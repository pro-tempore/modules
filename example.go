package main

import (
	"bytes"
	"flag"
	"html/template" // Every module should only create a text, not a full html. this is the job of the module.Handler
	"log"
	"net/rpc"
	"os"

	"gitlab.com/pro-tempore/modules/base"
	"gitlab.com/pro-tempore/server/types"
)

type Example struct {
	base.ModuleBase
}

const (
	MyPort = "30099"
)

var (
	T = template.Must(template.ParseFiles("Example.gohtml"))
)

func main() {
	flag.Parse() // required since you might want your own flags

	instance := new(Example)
	mBase := base.New(instance, MyPort)
	err := mBase.Start("Example")
	log.Println(err)
}

func (e *Example) Root(request types.RpcRequest, response *types.RpcResponse) error {
	// Renders modules page. Required.
	log.Println("Got request for Root!")
	var out bytes.Buffer
	varmap := map[string]interface{}{
		"content": "nothing",
	}

	err := T.ExecuteTemplate(&out, "content", varmap)
	if err != nil {
		log.Println(err)
	}
	response.Body = out.Bytes()

	return nil
}

func (e *Example) ExampleMethod(request types.RpcRequest, response *types.RpcResponse) error {
	// for API methods. Define your own, as many as you want.
	log.Println("Got request for ExampleMethod!")
	// How to access the database:

	client, err := rpc.Dial("tcp", e.Server+":"+base.DBPort)
	if err != nil {
		log.Println("Failed to dial server:", err)
		return err
	}

	query := types.NewQuery("SELECT * FROM users LIMIT 3")
	rows := make([]map[string]interface{}, 0)

	err = client.Call("DB.Query", query, &rows)
	if err != nil {
		log.Println("Database error:", err)
	}

	users := make([]types.User, 0)
	u := new(types.User)
	for _, r := range rows {
		base.ParseStruct(r, u)
		users = append(users, *u)
	}

	for _, item := range users {
		log.Println(item)
	}
	return err
}

func (e *Example) Shutdown(token string, dont_care *bool) error {
	// Do shutdown things here. Required.
	log.Println("Got call to shutdown!")
	os.Exit(0)
	return nil
}

func (e *Example) SetBase(b base.ModuleBase) {
	e.ModuleBase = b
}
