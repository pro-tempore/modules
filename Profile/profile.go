package main

import (
	"encoding/json"
	"net/http"
	"strconv"
	"text/template"

	"bytes"
	"flag"
	"log"
	"os"

	"gitlab.com/pro-tempore/modules/base"
	"gitlab.com/pro-tempore/server/types"
)

type Profile struct {
	base.ModuleBase
}

const (
	UPDATE_USER      = "UPDATE users SET name=$2, lastname=$3, email=$4, phone=$5, address=$6, city=$7, region=$8, zip=$9, country=$10 WHERE uid=$1"
	GET_USERINF      = "SELECT * FROM users WHERE uid=$1"
	GET_ROLE_COMPANY = "SELECT role, company FROM users WHERE email=$1"
)

var (
	MyPort = "30095"
)

func main() {
	flag.Parse()

	instance := new(Profile)
	mBase := base.New(instance, MyPort)
	err := mBase.Start("Profile")
	log.Println(err)

}
func (p *Profile) SetBase(b base.ModuleBase) {
	p.ModuleBase = b
}

func (p *Profile) Root(request types.RpcRequest, response *types.RpcResponse) error {

	uid := request.Cookies["uid"].(int)

	userInf, err := p.getUserInf(strconv.Itoa(uid))

	if err != nil {
		log.Println("error getting user info", err)
	}
	varmap := map[string]interface{}{ // This variable is used to get data into the template. for example populate tables and such according to the text template examples.
		"user": userInf, // Here we add an variable "timelogs" with the data ([]types.Geodata)
	}
	// WRITE OUT THE USERINF
	var out bytes.Buffer
	ModuleTemplate, err := template.ParseFiles("Templates/profile.gohtml")
	if err != nil {
		log.Println(err)
	}

	err = ModuleTemplate.ExecuteTemplate(&out, "content", varmap)
	if err != nil {
		log.Println(err)
	}
	response.Body = out.Bytes()

	return nil

}

func (p *Profile) getUserInf(uid string) (types.User, error) {
	rows, err := p.Query(GET_USERINF, uid)
	if err != nil {
		log.Println("Database error:", err)
		return types.User{}, err
	}
	user := new(types.User)
	base.ParseStruct(rows[0], user)

	return *user, nil
}

func (p *Profile) Update(request types.RpcRequest, response *types.RpcResponse) (err error) {
	log.SetPrefix("[Handlers.Profile/Update] ")

	uid := request.Cookies["uid"].(int)
	email := request.Cookies["email"].(string)

	user := new(types.User)
	err = json.Unmarshal(request.Body, user)
	if err != nil {
		log.Println("JSON error unmarshal", err)
		return
	}

	i := strconv.Itoa(uid)

	rows, err := p.Query("SELECT * FROM users WHERE uid !=$1", i)
	if err != nil {
		log.Println("Database error:", err)
		return
	}

	u := new(types.User)
	for _, r := range rows {
		base.ParseStruct(r, u)
		if u.Email == email {
			response.StatusCode = http.StatusFound
			return
		}
	}
	// no user with that email address found, continue

	rows, err = p.Query(UPDATE_USER,
		i,
		user.Name,
		user.Lastname,
		user.Email,
		user.Phone,
		user.Address,
		user.City,
		user.Region,
		user.Zip,
		user.Country,
	)

	if err != nil {
		log.Println("Failed to insert into database:", err)
		response.StatusCode = http.StatusInternalServerError
		return
	}

	response.StatusCode = http.StatusOK
	return
}

func (p *Profile) Shutdown(token string, dont_care *bool) error {
	// do shutdown things here. Required
	log.Println("Got call to shutdown!")
	os.Exit(0)
	return nil
}
